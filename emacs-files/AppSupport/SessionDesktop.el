;; -*- mode: emacs-lisp; coding: emacs-mule; -*-
;; --------------------------------------------------------------------------
;; Desktop File for Emacs
;; --------------------------------------------------------------------------
;; Created Tue Mar 11 22:46:36 2014
;; Desktop file format version 206
;; Emacs version 24.3.50.2
(setq revive:frame-configuration-to-restore
  '(progn 
     (revive:restore-frame '((tool-bar-position . top) (explicit-name) (icon-name) (top . 55) (left . 809) (unsplittable) (width . 105) (height . 52) (modeline . t) (fringe) (border-color . "black") (mouse-color . "Grey") (environment) (visibility . t) (cursor-color . "Red") (background-mode . dark) (horizontal-scroll-bars . t) (fullscreen) (alpha) (scroll-bar-width . 15) (cursor-type . box) (auto-lower) (auto-raise) (icon-type) (title) (buffer-predicate) (tool-bar-lines . 1) (menu-bar-lines . 1) (right-fringe . 12) (left-fringe . 4) (line-spacing) (background-color . "#262626") (foreground-color . "AntiqueWhite") (vertical-scroll-bars . right) (internal-border-width . 0) (border-width . 0) (font . "-apple-Courier_New-medium-normal-normal-*-14-*-*-*-m-0-iso10646-1") (fontsize . 0)) '(105 52 ((0 0 109 51)) (("~/Library/Application Support/Aquamacs Emacs/scratch buffer" "*scratch*" 1 1 ((tab "*scratch*" "/Users/pablito/Library/Application Support/Aquamacs Emacs/scratch buffer"))))))))

;; Global section:
(setq desktop-saved-frameset [frameset 1 (21279 59052 384971 0) (desktop . "206") "pablito@Pablo-Rosells-MacBook-Pro.local" nil nil ((((font-backend ns) (fontsize . 0) (font . "-apple-Courier_New-medium-normal-normal-*-14-*-*-*-m-0-iso10646-1") (border-width . 0) (internal-border-width . 0) (vertical-scroll-bars . right) (foreground-color . "AntiqueWhite") (background-color . "#262626") (line-spacing) (left-fringe . 4) (right-fringe . 12) (menu-bar-lines . 1) (tool-bar-lines . 1) (title) (icon-type) (auto-raise) (auto-lower) (cursor-type . box) (scroll-bar-width . 15) (alpha) (fullscreen) (horizontal-scroll-bars . t) (display-type . color) (background-mode . dark) (cursor-color . "Red") (visibility . t) (environment) (mouse-color . "Grey") (border-color . "black") (fringe) (modeline . t) (frameset--id . "F4B5-242B-3512-11EF") (frameset--mini t . t) (height . 52) (width . 105) (minibuffer . t) (unsplittable) (left . 809) (top . 55) (icon-name) (display . "Pablo-Rosells-MacBook-Pro.local") (explicit-name) (tool-bar-position . top)) ((min-height . 4) (min-width . 10) (min-height-ignore . 3) (min-width-ignore . 7) (min-height-safe . 1) (min-width-safe . 2)) leaf (total-height . 51) (total-width . 109) (normal-height . 1.0) (normal-width . 1.0) (buffer "*scratch*" (selected . t) (hscroll . 0) (fringes 4 12 nil) (margins nil) (scroll-bars 15 2 t nil) (vscroll . 0) (dedicated) (point . 1) (start . 1))))])
(setq desktop-missing-file-warning nil)
(setq tags-file-name nil)
(setq tags-table-list nil)
(setq search-ring '("libro" "xix" "\\foot" "Las" "myb" "wide" "\\widebar{H" "\\wide" "over" "\\overline{H" "G^" "\\mu^" "split" "array" "split" "Im\\"))
(setq regexp-search-ring '("\\(``\\)\\([^']\\)" "([^)]*)" "([^)]" "(" "\\\\begin{pro}\n[ ]*\\\\label{[^}]+}\\(\\\\textbf{[^}]+" "\n\n\\\\item" "^\n\\(\\\\end{[^}]+}\\)" "\n\n" "\\\\begin{[^{}]+}\n\n" "\\\\begin{" "^\n\n" "^\n\n+" "^\n" "\n" "{\\\\rm{\\([^��]+\\)" "{\\\\rm{\\("))
(setq register-alist nil)
(setq file-name-history '("/Volumes/MacHD2/IMATE/papirhos-FB/presentacion.txt" "/Volumes/MacHD2/git-projects/papirhos-amss-1-pablo/../../IMATE/papirhos-FB/presentacion.html" "/Volumes/MacHD2/git-projects/papirhos-amss-1-pablo/presentacion.tex" "/Volumes/MacHD2/git-projects/CV/plan-de-trabajo-2014.tex" "/Volumes/MacHD2/Dropbox/ITAM/GeometriaAnalitica/parciales/parcial2.tex" "/Volumes/MacHD2/Dropbox/ITAM/GeometriaAnalitica/parciales/parcial1.tex" "/Volumes/MacHD2/git-projects/CV/plan-de-trabajo-2014.tex" "/Volumes/MacHD2/git-projects/CV/CurriMATEM.tex" "/Volumes/MacHD2/IMATE/papirhos-mca/figuras-tikz/evnbolacer.tikz" "~/Desktop/Editorial/separadores/separadores.tex" "~/git-projects/papirhos-amss-1-pablo/meta.yaml" "~/git-projects/papirhos-trazos-mineria/meta.yaml" "~/IMATE/isbn-Grupos-I.tex" "~/git-projects/papirhos-trazos-mineria/legal.tex" "~/IMATE/isbn-Dos-o-tres-trazos.tex" "~/IMATE/isbn-prueba.tex" "~/git-projects/papirhos-trazos-mineria/Rakefile" "~/git-projects/papirhos-trazos-mineria/trazos.tex" "~/git-projects/papirhos-trazos-mineria/Rakefile" "~/git-projects/papirhos-trazos-mineria/meta.yaml" "~/git-projects/papirhos-trazos-mineria/Rakefile" "~/git-projects/papirhos-trazos-mineria/meta.yaml" "~/git-projects/papirhos-trazos-mineria/../papirhos-amss-1-pablo/meta.yaml" "~/git-projects/papirhos-amss-1-pablo/../papirhos-trazos-mineria/meta.yaml" "~/git-projects/papirhos-amss-1-pablo/../papirhos-trazos-mineria/legal.tex" "~/git-projects/papirhos-trazos-mineria/../papirhos-amss-1-pablo/legal.tex" "~/git-projects/papirhos-trazos-mineria/legal.tex" "~/git-projects/papirhos-trazos-mineria/meta.yaml" "~/git-projects/papirhos-trazos-mineria/editorial.tex" "/Volumes/MacHD2/git-projects/papirhos-amss-1-pablo/editorial.tex"))

;; Buffer section -- buffers listed in same order as in buffer list:
(desktop-create-buffer 206
  "/Users/pablito/Library/Application Support/Aquamacs Emacs/scratch buffer"
  "*scratch*"
  'text-mode
  '(visual-line-mode smart-spacing-mode linum-mode)
  1
  '(nil nil)
  nil
  nil
  '((buffer-file-coding-system . utf-8) (truncate-lines)))

