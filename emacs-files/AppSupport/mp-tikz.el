;;define the mode
(define-derived-mode mp-tikz-mode latex-mode
  "tikz mode"
  "Major mode for editing tikz pictures"
;  (setq font-lock-defaults '((mp-tikz-lock-keywords)))
;  (setq mp-tikz-keywords-regexp nil)
)

(provide 'mp-tikz-mode)

(defun mp-tikz-to-pdf ()
  (interactive)
  (shell-command (concat "rake -f tikztopdf.rb tikzfile[" (file-name-sans-extension (file-name-nondirectory (buffer-file-name))) "] >>/dev/null 2>&1")))

(defun mp-clean-tikz-rake ()
  (interactive)
  (shell-command "rake -f tikztopdf.rb clean >>/dev/null 2>&1"))

(defun mp-open-tikz-pdf ()
  (interactive)
(shell-command (concat "open -a /Applications/Preview.app " (file-name-sans-extension (file-name-nondirectory (buffer-file-name))) ".pdf" )))	
(defun my-tikz-hook ()
  (local-set-key "\C-cc" 'mp-tikz-to-pdf)
  (local-set-key "\C-cd" 'mp-clean-tikz-rake)
  (local-set-key "\C-cv" 'mp-open-tikz-pdf))

(add-hook 'mp-tikz-mode-hook 'my-tikz-hook)

(add-hook 'mp-tikz-mode
	  (lambda ()
	    (font-lock-add-keywords nil
			   '(("\\\\coordinate\\>\\|\\\\draw\\>" 0 font-lock-keyword-face t))
)))

(add-hook 'mp-tikz-mode-hook
	  (lambda ()
	    (font-lock-add-keywords nil
			   '(("\\(\\[\[^][\]*\\]\\)" 0 font-lock-variable-name-face t))
)))

(add-hook 'mp-tikz-mode-hook
	  (lambda ()
	    (font-lock-add-keywords nil
			   '(("\\(\(\[^()\]*\)\\)" 0 font-lock-builtin-face t))
)))

(add-hook 'mp-tikz-mode-hook
	  (lambda ()
	    (font-lock-add-keywords nil
			   '(("\\(\\%\[^\n\]*\\)" 0 font-lock-comment-face t))
)))

(font-lock-add-keywords
 'mp-tikz-mode
 `((,(concat "\\<at\\>\\|"
	     "\\<node\\>\\|"
	     "\\<plot\\>\\|"
	     "\\\\tikzstyle\\>")
    (0 'font-lock-keyword-face append))))

;(add-hook 'mp-tikz-mode-hook
;      (lambda ()
;        (TeX-add-symbols "draw")))

