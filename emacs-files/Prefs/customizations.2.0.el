(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 208 t)
 '(aquamacs-tool-bar-user-customization nil t)
 '(default-frame-alist (quote ((cursor-type . box) (vertical-scroll-bars . right) (internal-border-width . 0) (modeline . t) (fringe) (mouse-color . "black") (background-mode . dark) (tool-bar-lines . 1) (menu-bar-lines . 1) (right-fringe . 12) (left-fringe . 4) (cursor-color . "Red") (background-color . "#161d2a") (foreground-color . "#c6c6c6") (font . "-apple-Lucida_Grande-medium-normal-normal-*-13-*-*-*-p-0-iso10646-1") (fontsize . 0) (font-backend ns))))
 '(ns-tool-bar-display-mode (quote both) t)
 '(ns-tool-bar-size-mode (quote regular) t)
 '(safe-local-variable-values (quote ((cssm-indent-level . 2))))
 '(visual-line-mode nil t))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(latex-mode-default ((t (:inherit text-mode-default :stipple nil :background "#161d2a" :foreground "#c6c6c6" :strike-through nil :underline nil :slant normal :weight normal :height 140 :width normal :foundry "Handwriting " :family "Courier New")))))
