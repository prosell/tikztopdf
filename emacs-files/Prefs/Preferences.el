;; This is the Aquamacs Preferences file.
;; Add Emacs-Lisp code here that should be executed whenever
;; you start Aquamacs Emacs. If errors occur, Aquamacs will stop
;; evaluating this file and print errors in the *Messags* buffer.
;; Use this file in place of ~/.emacs (which is loaded as well.)
;; (setq font-lock-maximum-decoration t)
;;(setq auto-mode-alist
;;      (append '(("\\.cls" . latex-mode)) auto-mode-alist))

;;

(setq auto-mode-alist
      (append '(("\\.yaml" . yaml-mode)) auto-mode-alist))

(load "mp-tikz")
(require 'mp-tikz-mode)
(setq auto-mode-alist
      (append '(("\\.tikz" . mp-tikz-mode)) auto-mode-alist))

;(add-hook 'mp-tikz-mode-hook 'linum-on)
;(add-hook 'LaTeX-mode-hook 'linum-on)

(require 'linum)
(line-number-mode 1)
(column-number-mode 1) ;; Line numbers on left most column
(global-linum-mode 1)

(eval-after-load "ispell"
  (progn
    (setq ispell-extra-args '("-w" "áéíóúÁÉÍÓÚüÜ")
          ispell-dictionary "es"
          ispell-silently-savep t)))
(setq-default ispell-program-name "aspell")

(require 'ispell)
;; Starts the Emacs server
(server-start)

;(defun raise-emacs-on-aqua() 
;  (shell-command "osascript -e 'tell application \"Emacs\" to activate' &"))
;(add-hook 'server-switch-hook 'raise-emacs-on-aqua)

;; The following only works with AUCTeX loaded
;(require 'tex-site)
;(add-hook 'TeX-mode-hook
;	  (lambda ()
;	    (add-to-list 'TeX-output-view-style
;			 '("^pdf$" "."
;			   "/Applications/PDFView.app/Contents/MacOS/gotoline.sh %n %(OutFullPath)"))
;	    (add-to-list 'TeX-expand-list
;			 '("%(OutFullPath)" (lambda nil
;					      (expand-file-name
;					       (TeX-active-master (TeX-output-extension) t)
;					       (TeX-master-directory))))))
;	  )		
