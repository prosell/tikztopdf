# -*- mode: ruby; coding: utf-8; -*-

desc "Agrega el encabezado y final a los archivos tikz para compilar en latex"
task :tikztotex do
  TIKZSRC = FileList['*.tikz']
  TIKZSRC.each do |t|
    source = ""
    obj = t.sub(/\.[^.]*$/, '')
    source = File.read("pre.tex") + File.read("#{t}") + File.read("post.tex")
    objective = "temp-" + "#{obj}" + ".tex"
    puts "#{objective} Creado.\n"
    File.open(objective, 'w'){ |file|
        file.write(source)
      }
  end
end

desc "Compila los archivos generados por :tikztotex"
task :texit do
  TEXSRC = FileList['temp-*.tex']
  TEXSRC.each do |t|
    sh "pdflatex -interaction=batchmode -halt-on-error #{t}"    
  end
end

desc "Recorta los pdfs generados por :texit"
task :cropit do
  PDFSRC = FileList['temp-*.pdf']
  PDFSRC.each do |t|
    obj = t.sub('temp-', '')
    sh "pdfcrop --margins 1 #{t} #{obj}" 
    puts "#{obj} Generado."
  end
end


task :tikzfile, [:filename] do |t, args|
  source = ""
  source = File.read("pre.tex") + File.read("#{args.filename}.tikz") + File.read("post.tex")
  objective = "temp-" + "#{args.filename}" + ".tex"
   File.open(objective, 'w'){ |file|
      file.write(source)
    }
   sh "pdflatex -interaction=batchmode -halt-on-error -jobname=#{args.filename}-full #{objective}"  
   sh "pdfcrop --margins 1 #{args.filename}-full.pdf #{args.filename}.pdf" 
   puts 'Listo!'               
end


task :clean do
  sh 'rm -f *.aux temp-* *.log *.pdf'
  puts 'Archivos eliminados.'
end

task :cleantemp do
  sh 'rm -f *.aux temp-* *.log'
  puts 'Archivos eliminados.'
end

task :cleanrun do
  sh 'rm -f *.aux temp-* *.log'
  OUTPUTFILES = FileList['*.pdf']
  puts "\nImágenes generadas:\n\n"
  OUTPUTFILES.each do |t|
    puts "#{t}\n"
  end
  puts "\n"
end

task :texit => :tikztotex
task :cropit => :texit
task :cleanrun => :cropit

task :default => :cleanrun

